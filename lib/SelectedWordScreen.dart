import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';

class SelectedWordScreen extends StatelessWidget {
  SelectedWordScreen({Key key, @required this.selectedWordSet})
      : super(key: key);

  final Set<WordPair> selectedWordSet;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(title: Text('Selected Word')),
      body: ListView(
        children: getListTiles(context),
      ),
    );
  }

  List<Widget> getListTiles(BuildContext context) {
    final Iterable<ListTile> tiles = selectedWordSet.map((WordPair wordPair) {
      return new ListTile(
        title: new Text(wordPair.asCamelCase),
      );
    });
    return ListTile.divideTiles(tiles: tiles, context: context).toList();
  }
}

import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';

class ListItemUtils {
  static Widget renderItem(
      WordPair wordPair,
      bool getFavoriteStatus(WordPair wordPair),
      Function(WordPair) editFavoriteStatus) {
    return ListTile(
      title: Text(
        wordPair.asCamelCase,
        style: const TextStyle(fontSize: 18),
      ),
      subtitle: Text(
        wordPair.asCamelCase + " subtitle",
        style: const TextStyle(fontSize: 12),
      ),
      trailing: new Icon(
        getFavoriteStatus(wordPair) ? Icons.favorite : Icons.favorite_border,
        color: Colors.blue,
      ),
      onTap: () => editFavoriteStatus(wordPair),
    );
  }
}

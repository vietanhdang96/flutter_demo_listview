import 'package:demo_listview/ListItemUtils.dart';
import 'package:demo_listview/SelectedWordScreen.dart';
import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<WordPair> _wordList = <WordPair>[];
  final Set<WordPair> _selectedWordSet = new Set<WordPair>();

  void _addNewWordToWordList() {
    setState(() {
      _wordList.addAll(generateWordPairs().take(10));
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.list),
            onPressed: _navigateToSelectedItemScreen,
          )
        ],
      ),
      body: Center(
        child: _renderList(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _addNewWordToWordList,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget _renderList() {
    if (_wordList.length > 0) {
      return ListView.builder(
          itemCount: _wordList.length,
          itemBuilder: (buildContext, index) {
            return ListItemUtils.renderItem(
                _wordList[index], getFavoriteStatus, editFavoriteStatus);
          });
    } else {
      return Text('empty list');
    }
  }

  void _navigateToSelectedItemScreen() {
    Navigator.of(context)
        .push(new MaterialPageRoute(builder: (BuildContext context) {
      return SelectedWordScreen(
        selectedWordSet: _selectedWordSet,
      );
    }));
  }

  void editFavoriteStatus(WordPair wordPair) {
    setState(() {
      getFavoriteStatus(wordPair)
          ? _selectedWordSet.remove(wordPair)
          : _selectedWordSet.add(wordPair);
    });
  }

  bool getFavoriteStatus(WordPair wordPair) {
    return _selectedWordSet.contains(wordPair);
  }
}
